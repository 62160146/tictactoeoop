
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ii drunkboy
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    static int count = 0;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col: ");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                count++;
                break;
            }
            System.out.println("Error: Table at row and col is not empty!!!");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                if (table.getWinner() == null || count==9) {
                    System.out.println("Draw!!!");
                } else {
                    System.out.println(table.getWinner().getName() + " Win!!"+" "+count);
                }
                break;
            }
            table.switchPlayer();
        }
    }
}
