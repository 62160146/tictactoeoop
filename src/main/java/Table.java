/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ii drunkboy
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int lastcol;
    private int lastrow;
    private Player winner;
    private boolean Finish = false;
    

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
        setStatwWnLose();
    }

    private void setStatwWnLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        Finish = true;
        winner = currentPlayer;
        setStatwWnLose();
    }

    void checkX() {
        if ((table[0][0] == table[1][1] && table[1][1] == table[2][2]
                && table[2][2] == currentPlayer.getName()) || (table[2][0]
                == table[1][1] && table[1][1] == table[0][2]
                && table[0][2] == currentPlayer.getName())) {
            Finish = true;
            winner = currentPlayer;
        }
        return;
    }

    void checkDraw() {
        if (winner == null && Game.count==9 ) {
            playerX.draw();
            playerO.draw();
            Finish = true;
        }

    }

    void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();

    }

    public boolean isFinish() {
        return Finish;
    }

    public Player getWinner() {
        return winner;

    }
}
